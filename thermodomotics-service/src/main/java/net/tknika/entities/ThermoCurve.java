package net.tknika.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({ @NamedQuery(name = "ThermoCurve.deleteAll", query = "delete from ThermoCurve") })
public class ThermoCurve implements Serializable {

    private static final long serialVersionUID = -7206840606670807737L;

    @Id
    private Room room;
    @ElementCollection
    @Temporal(TemporalType.TIMESTAMP)
    private List<Date> dates;
    @ElementCollection
    private List<Double> temperatures;
    private boolean completed;

    @SuppressWarnings("unused")
    private ThermoCurve() {
    }

    public ThermoCurve(Room room) {
        this.room = room;
    }

    public Room getRoom() {
        return room;
    }

    public List<Date> getDates() {
        if (dates == null) {
            dates = new ArrayList<Date>();
        }
        return dates;
    }

    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

    public List<Double> getTemperatures() {
        if (temperatures == null) {
            temperatures = new ArrayList<Double>();
        }
        return temperatures;
    }

    public void setTemperatures(List<Double> temperatures) {
        this.temperatures = temperatures;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

}
