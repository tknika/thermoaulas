package net.tknika.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
        @NamedQuery(name = "TrainingData.findAll", query = "select td from TrainingData td"),
        @NamedQuery(name = "TrainingData.deleteAll", query = "delete from TrainingData"),
        @NamedQuery(name = "TrainingData.deleteRoomsData", query = "delete from TrainingData td where td.room = :room"),
        @NamedQuery(name = "TrainingData.find", query = "select td from TrainingData td where td.room = :room and td.start = :start"),
        @NamedQuery(name = "TrainingData.findFromTo", query = "select td from TrainingData td "
                + "where td.room = :room and td.start >= :start and td.end <= :end"),
        @NamedQuery(name = "TrainingData.findAllForRoom", query = "select td from TrainingData td where td.room = :room")

})
@IdClass(TrainingDataId.class)
public class TrainingData {

    @Id
    private Room room;
    @Id
    private long start;
    private Double initialTemperature;
    private long end;
    private Double finalTemperature;
    private Double externalTemperature;

    private double elapsedTime;

    public TrainingData() {
        super();
    }

    public TrainingData(Room room, long start) {
        super();
        this.room = room;
        this.start = start;
    }

    public Room getRoom() {
        return room;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public Double getInitialTemperature() {
        return initialTemperature;
    }

    public void setInitialTemperature(Double initialTemperature) {
        this.initialTemperature = initialTemperature;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public Double getFinalTemperature() {
        return finalTemperature;
    }

    public void setFinalTemperature(Double finalTemperature) {
        this.finalTemperature = finalTemperature;
    }

    public Double getExternalTemperature() {
        return externalTemperature;
    }

    public void setExternalTemperature(Double externalTemperature) {
        this.externalTemperature = externalTemperature;
    }

    public double getTimeSpentInMinutes() {
        return ((end - start) / (1000.0 * 60));
    }

    public final double getElapsedTime() {
        return elapsedTime;
    }

    public final void setElapsedTime(double elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    @Override
    public String toString() {
        return "initTemp: " + initialTemperature + "-endTemp: " + finalTemperature + "-timeSpent:"
                + getTimeSpentInMinutes() + "-externalTemp:" + externalTemperature;
    }

}
