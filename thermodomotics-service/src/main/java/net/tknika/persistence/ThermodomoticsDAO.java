package net.tknika.persistence;

import java.util.List;

import net.tknika.entities.Room;
import net.tknika.entities.ThermoCurve;
import net.tknika.entities.TrainingData;

public interface ThermodomoticsDAO {

    List<Room> getRooms();

    List<TrainingData> getTrainingData(Room room);

    Room getRoom(String roomId);

    ThermoCurve getCurve(Room room);

}
