package net.tknika.domo.service.thermo.task.check;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import net.tknika.domo.Config;
import net.tknika.domo.model.DaiaClassifier;
import net.tknika.domo.model.DomoticData;
import net.tknika.domo.model.ExecutionPriority;
import net.tknika.domo.model.Reservation;
import net.tknika.domo.model.Room;
import net.tknika.domo.service.thermo.task.RoomTask;
import net.tknika.domo.service.thermo.task.TurnOnHeating;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

public class TurnOn extends RoomTask {

    private static final Logger logger = Logger.getLogger(TurnOn.class);
    private static final String THREAD_NAME_SUFFIX = "turn-on";

    private Reservation reservation;

    int desiredTemperature;

    int timeIntervalInMinutes;

    private void init() {
        Properties p = new Properties();
        try {
            p.load(getClass().getClassLoader().getResourceAsStream(Config.File));
            try {
                desiredTemperature = Integer.parseInt(p.getProperty("domotic.desiredTemperature"));
            } catch (Exception e) {
            }
            try {
                timeIntervalInMinutes = Integer.parseInt(p.getProperty("domotic.tempCheckingTimeIntervalInMinutes"));
            } catch (Exception e) {
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public TurnOn() {
        init();
    }

    public TurnOn(Room room, Reservation reservation) {
        super(room);
        this.reservation = reservation;
        init();
    }

    public TurnOn(Room room, Date execution, ExecutionPriority priority, Reservation reservation) {
        super(room, execution, priority);
        this.reservation = reservation;
        init();
    }

    public TurnOn(Room room, Date execution, Reservation reservation) {
        super(room, execution);
        this.reservation = reservation;
        init();
    }

    public TurnOn(Room room, Date execution) {
        super(room, execution);
        init();
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    @Override
    protected String getThreadNameSuffix() {
        return THREAD_NAME_SUFFIX;
    }

    @Override
    protected void execute() {
        logger.info("executing task that checks if heating must start");
        try {
            DomoticData data = deviceService.getRoomData(reservation.getRoom());
            if (data != null) {
                logger.info("Current temperature: " + data.getTemperature());
                DaiaClassifier classifier = new DaiaClassifier(); // .getContextualReference(DaiaClassifier.class,
                                                                  // false);
                classifier.setReservation(reservation);
                int timeInMilliseconds = (int) classifier.getTimeToHeatOn(data.getTemperature(), desiredTemperature,
                        10);
                logger.info("room needs to start heating " + (int) (timeInMilliseconds / (1000 * 60))
                        + " minutes before event");
                Date mustStart = DateUtils.addMilliseconds(reservation.getStart(), -timeInMilliseconds);
                if (new Date().after(mustStart)) {
                    TurnOnHeating turnOn = new TurnOnHeating(getRoom(), mustStart); // thermodomoticService.newTask(TurnOnHeating.class,
                                                                                    // mustStart, getRoom());
                    turnOn.setReservation(reservation);
                    thermodomoticService.execute(turnOn);
                    Date date = DateUtils.addMinutes(mustStart, timeIntervalInMinutes);
                    StopHeating stopHeat = new StopHeating(getRoom(), date); // .newTask(StopHeating.class, date,
                                                                             // getRoom());
                    stopHeat.setReservation(reservation);
                    stopHeat.setTrainingDataStartTime(mustStart.getTime());
                    thermodomoticService.execute(stopHeat);
                } else {
                    TurnOn repeatTask = new TurnOn(getRoom(), mustStart); // thermodomoticService.newTask(TurnOn.class,
                                                                          // mustStart, getRoom());
                    repeatTask.setReservation(reservation);
                    thermodomoticService.execute(repeatTask);
                }
            }
        } catch (Exception e) {
            logger.error("Terminated with errors", e);
        }
    }
}
