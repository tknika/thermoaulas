package net.tknika.domo.service.thermo.task.check;

import java.util.Date;

import net.tknika.domo.model.CentralHeating;
import net.tknika.domo.model.DomoticData;
import net.tknika.domo.model.ExecutionPriority;
import net.tknika.domo.model.Room;
import net.tknika.domo.model.ThermoCurve;
import net.tknika.domo.service.thermo.task.RoomTask;

import org.apache.log4j.Logger;

public class GetCurveData extends RoomTask {

    private static final Logger logger = Logger.getLogger(GetCurveData.class);
    private static final String THREAD_NAME_SUFFIX = "get-curve-data";

    public GetCurveData() {
        super();
    }

    public GetCurveData(Room room) {
        super(room);

    }

    public GetCurveData(Room room, Date execution) {
        super(room, execution);

    }

    public GetCurveData(Room room, Date execution, ExecutionPriority priority) {
        super(room, execution, priority);

    }

    @Override
    protected String getThreadNameSuffix() {
        return THREAD_NAME_SUFFIX;
    }

    @Override
    protected void execute() {
        logger.info("executing get curve data");
        try {
            ThermoCurve curve = dao.getCurve(getRoom());
            Room room = getRoom();
            boolean firstValue = false;
            if (curve == null) {
                curve = new ThermoCurve(room);
                firstValue = true;
            }
            DomoticData data = deviceService.getRoomData(getRoom());
            Double extTemp = deviceService.getOutsideTemperature();

            if (CentralHeating.isOn(extTemp)) {
                if (data != null) {
                    logger.info("Current temperature: " + data.getTemperature());
                    curve.getDates().add(data.getDatetime());
                    curve.getTemperatures().add(data.getTemperature());
                    if (firstValue) {
                        dao.save(curve);
                    } else {
                        dao.update(curve);
                    }
                }
            } else {
                logger.info("Not valid data for heating curve generation because central heating is off...");
            }
        } catch (Exception e) {
            logger.error("Terminated with errors", e);
        }
    }
}
