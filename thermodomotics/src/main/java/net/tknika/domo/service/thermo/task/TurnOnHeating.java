package net.tknika.domo.service.thermo.task;

import java.util.Date;

import net.tknika.domo.model.DomoticData;
import net.tknika.domo.model.ExecutionPriority;
import net.tknika.domo.model.Reservation;
import net.tknika.domo.model.Room;
import net.tknika.domo.model.TrainingData;
import net.tknika.domo.service.device.DeviceService;
import net.tknika.domo.service.device.zwave.OpenhabServiceImpl;

import org.apache.log4j.Logger;

public class TurnOnHeating extends RoomTask {

    private static final Logger logger = Logger.getLogger(TurnOnHeating.class);
    private static final String THREAD_NAME_SUFFIX = "heat-on";

    private Reservation reservation;

    public TurnOnHeating() {
    }

    public TurnOnHeating(Room room, Date execution, ExecutionPriority priority, Reservation reservation) {
        super(room, execution, priority);
        this.reservation = reservation;
    }

    public TurnOnHeating(Room room, Date execution) {
        super(room, execution);
    }

    public TurnOnHeating(Room room, Date execution, ExecutionPriority priority) {
        super(room, execution, priority);
    }

    public TurnOnHeating(Room room, Date execution, Reservation reservation) {
        super(room, execution);
        this.reservation = reservation;
    }

    public TurnOnHeating(Room room, Reservation reservation) {
        super(room);
        this.reservation = reservation;
    }

    public TurnOnHeating(Reservation reservation) {
        super(reservation.getRoom());
        this.reservation = reservation;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    @Override
    protected String getThreadNameSuffix() {
        return THREAD_NAME_SUFFIX;
    }

    @Override
    public void execute() {
        logger.info("executing turn on heat");
        DeviceService deviceService = new OpenhabServiceImpl(); // BeanProvider.getContextualReference(DeviceService.class);
        deviceService.turnOnHeating(getRoom());
        if (reservation != null) {
            TrainingData td = new TrainingData(getRoom(), getExecution().getTime());
            DomoticData data = deviceService.getRoomData(getRoom());
            if (data != null) {
                logger.info("Current temperature: " + data.getTemperature());
                td.setInitialTemperature(data.getTemperature());
                td.setExternalTemperature(data.getExternalTemperature());
                if (dao.getTrainingData(getRoom(), getExecution()) != null) {
                    logger.error("Allready exists trainingData for this reservation heating on");
                } else {
                    dao.save(td);
                    logger.info("saved initial td with start " + getExecution());
                }
            }
        }
    }

}
