package net.tknika.domo.service.reservation.zimbra;

import java.util.List;

import javax.enterprise.inject.Alternative;
import javax.inject.Singleton;

import net.tknika.domo.model.Reservation;
import net.tknika.domo.model.ReservationFilter;
import net.tknika.domo.model.Room;
import net.tknika.domo.service.reservation.ReservationService;

@Alternative
@ZimbraService
@Singleton
public class ZimbraServiceImpl implements ReservationService {

    @Override
    public List<Reservation> getReservations() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void deleteReservations() {
        // TODO Auto-generated method stub
    }

    @Override
    public List<Reservation> getReservations(ReservationFilter filter) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Reservation getReservation(String roomId, long date) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void makeReservation(Reservation reservation) {
        // TODO Auto-generated method stub
    }

    @Override
    public void deleteReservation(String roomId) {
        // TODO Auto-generated method stub
    }

    @Override
    public List<Reservation> getReservations(Room room) {
        // TODO Auto-generated method stub
        return null;
    }

}
