package net.tknika.domo.service.thermo.task;

import static net.tknika.domo.model.ExecutionPriority.NORMAL;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.tknika.domo.model.ExecutionPriority;
import net.tknika.domo.model.Room;

import org.apache.log4j.Logger;

public abstract class RoomTask extends ThermoTask {

    private static final Logger logger = Logger.getLogger(RoomTask.class);

    private Room room;

    public RoomTask() {
        super();
    }

    public RoomTask(Room room) {
        this(room, new Date());
    }

    public RoomTask(Room room, Date execution) {
        this(room, execution, NORMAL);
    }

    public RoomTask(Room room, Date execution, ExecutionPriority priority) {
        super(execution, priority);
        this.room = room;
    }

    public final Room getRoom() {
        return room;
    }

    public final void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public final void run() {
        String previousThreadName = Thread.currentThread().getName();
        try {
            StringBuilder threadName = new StringBuilder();
            threadName.append("room-").append(room.getName()).append("-").append(getThreadNameSuffix());
            Thread.currentThread().setName(threadName.toString());
            logger.info("Running...");
            execute();
            logger.info("Terminated");
        } catch (Exception e) {
            logger.error("Terminated with errors", e);
            // TODO notify error
        } finally {
            Thread.currentThread().setName(previousThreadName);
        }
    }

    protected abstract String getThreadNameSuffix();

    protected abstract void execute();

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(getExecution()));
        sb.append("- ").append(room);
        sb.append("- ").append(this.getClass().getSimpleName());
        return sb.toString();
    }

}
