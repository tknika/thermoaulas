package net.tknika.domo.service.thermo.task;

import static net.tknika.domo.model.ExecutionPriority.NORMAL;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import net.tknika.domo.Config;
import net.tknika.domo.model.ExecutionPriority;
import net.tknika.domo.model.Reservation;
import net.tknika.domo.model.ReservationDates;
import net.tknika.domo.model.ReservationFilter;
import net.tknika.domo.model.Room;
import net.tknika.domo.service.thermo.task.check.GetCurveData;
import net.tknika.domo.service.thermo.task.check.TurnOn;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

public class MainTask extends ThermoTask {

    private static final Logger logger = Logger.getLogger(MainTask.class);
    private static final String THREAD_NAME_SUFFIX = "main";
    private static final int MsInMinute = 60000;
    private static final long merge_tolerance_time = 5000; // 5s

    private int desiredTemp;
    private int collectTimeInMinutes;
    private int collectIntervalInMinutes;
    private int hoursBefore;
    private int checkEachMinutes;
    private int startAllMinutesBefore;
    private int finishAllMinutesLater;

    public MainTask() {
        this(new Date(), NORMAL);
    }

    public MainTask(Date execution) {
        this(execution, NORMAL);
    }

    public MainTask(Date execution, ExecutionPriority priority) {
        super(execution, priority);
        init();
    }

    private void init() {
        Properties p = new Properties();
        try {
            p.load(getClass().getClassLoader().getResourceAsStream(Config.File));

            try {
                desiredTemp = Integer.parseInt(p.getProperty("domotic.desiredTemp"));
            } catch (Exception e) {
            }
            try {
                collectTimeInMinutes = Integer.parseInt(p.getProperty("domotic.collectTimeInMinutes"));
            } catch (Exception e) {
            }
            try {
                collectIntervalInMinutes = Integer.parseInt(p.getProperty("domotic.collectIntervalInMinutes"));
            } catch (Exception e) {
            }
            try {
                hoursBefore = Integer.parseInt(p.getProperty("domotic.advanceHours"));
            } catch (Exception e) {
            }
            try {
                checkEachMinutes = Integer.parseInt(p.getProperty("domotic.reservationCheckEachMinutes"));
            } catch (Exception e) {
            }
            try {
                startAllMinutesBefore = Integer.parseInt(p.getProperty("domotic.startAllMinutesBefore"));
            } catch (Exception e) {
            }
            try {
                finishAllMinutesLater = Integer.parseInt(p.getProperty("domotic.finishAllMinutesLater"));
            } catch (Exception e) {
            }
        } catch (Exception e) {
            logger.error("MainTask:init " + e.getMessage());
        }
    }

    protected String getThreadNameSuffix() {
        return THREAD_NAME_SUFFIX;
    }

    protected void execute() {
        logger.info("-------------------");
        logger.info("Executing main task");
        logger.info("-------------------");

        try {
            ReservationFilter filter = new ReservationFilter();
            Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
            Date tomorrow = DateUtils.addDays(today, 1);
            Date now = new Date();
            // long nowMs = now.getTime();
            tomorrow = DateUtils.addSeconds(tomorrow, -1);
            filter.setFrom(today);
            filter.setTo(tomorrow);
            List<Reservation> reservations = reservationService.getReservations(filter);
            List<Date> dates = new ArrayList<Date>();

            Map<String, Room> rooms = new HashMap<String, Room>();
            // Map<String, List<Order>> ordersByRoomName = new HashMap<String, List<Order>>();
            for (Reservation rsv : reservations) {
                dates.add(rsv.getStart());
                dates.add(rsv.getEnd());
                rooms.put(rsv.getRoom().getName(), rsv.getRoom());
                // rooms.add(rsv.getRoom());
            }
            logger.info("Number of rooms: " + rooms.size());
            ReservationDates storedDates = dao.getReservationDates();
            logger.info("storedDates: " + storedDates);
            logger.info("current dates: " + dates);
            if (storedDates == null || !storedDates.equals(dates)) {
                if (storedDates == null) {
                    logger.info("storedDates == null");
                    storedDates = new ReservationDates();

                } else {
                    logger.info("storedDates != null");
                }

                storedDates.setDates(dates);
                dao.save(storedDates);

                thermodomoticService.shutDown();
                thermodomoticService.reanimate();

                reservations = mergeReservations(reservations);

                logger.info("launch turn off heating for each room if heating is ON");
                for (Room room : rooms.values()) {

                    TurnOffHeating turnOffHeating = new TurnOffHeating(room, new Date(), ExecutionPriority.NORMAL); // thermodomoticService.newTask(TurnOffHeating.class,
                                                                                                                    // room);
                    turnOffHeating.setNotNormalTermination(true);
                    thermodomoticService.execute(turnOffHeating);

                }

                // Turn off All for rooms not reserved at this moment
                Map<String, Room> roomsNotInUse = new HashMap<String, Room>(rooms.size());
                for (Room room : rooms.values())
                    roomsNotInUse.put(room.getName(), room);

                for (Reservation rsv : reservations) {

                    if (now.after(new Date(rsv.getStart().getTime()))// - startAllMinutesBefore * MsInMinute))
                            && now.before(new Date(rsv.getEnd().getTime())))// + finishAllMinutesLater * MsInMinute)))
                        roomsNotInUse.remove(rsv.getRoom().getName()); // Only rooms not in use at this moment are taken
                                                                       // into account

                    // Add virtual order to take into account this turn on
                    // addOrder(rsv.getRoom().getName(), ordersByRoomName, nowMs, null, TurnOnAllExceptHeat.class);

                }

                logger.info("Launch turn off all for rooms not in use");
                for (Room room : roomsNotInUse.values()) {
                    TurnOffAll turnOffAll = new TurnOffAll(room, new Date(), ExecutionPriority.NORMAL); // .newTask(TurnOffAll.class,
                                                                                                        // room);
                    thermodomoticService.execute(turnOffAll);
                    // addOrder(room.getName(), ordersByRoomName, nowMs, turnOffAll, TurnOffAll.class);

                }

                // launch tasks for each reservation (old reservation won't be considered; in progress reservations yes)
                for (Reservation rsv : reservations) {
                    if (now.after(new Date(rsv.getEnd().getTime())))// + finishAllMinutesLater * MsInMinute)))
                        continue;
                    Date taskStart = DateUtils.addHours(rsv.getStart(), -hoursBefore);
                    boolean isFirstReservation = dao.getCurve(rsv.getRoom()) == null;
                    if (isFirstReservation) {
                        TurnOnHeating turnOnHeating = new TurnOnHeating(rsv.getRoom(), taskStart,
                                ExecutionPriority.NORMAL); // thermodomoticService.newTask(TurnOnHeating.class,
                                                           // taskStart,rsv.getRoom());
                        turnOnHeating.setReservation(rsv);
                        thermodomoticService.execute(turnOnHeating);

                        // addOrder(rsv.getRoom().getName(), ordersByRoomName, taskStart.getTime(), turnOnHeating,
                        // TurnOnHeating.class);
                        Date curveDate = taskStart;
                        Date endCurveData = DateUtils.addMinutes(taskStart, collectTimeInMinutes);
                        GetCurveData curveDataTask;
                        while (curveDate.before(endCurveData)) {
                            if (!curveDate.before(new Date())) {
                                curveDataTask = new GetCurveData(rsv.getRoom(), curveDate); // thermodomoticService.newTask(GetCurveData.class,
                                                                                            // curveDate,rsv.getRoom());
                                thermodomoticService.execute(curveDataTask);
                                // addOrder(rsv.getRoom().getName(), ordersByRoomName, curveDate.getTime(),
                                // curveDataTask,
                                // GetCurveData.class);
                            }
                            curveDate = DateUtils.addMinutes(curveDate, collectIntervalInMinutes);
                        }
                        TurnOn turnOn = new TurnOn(rsv.getRoom(), curveDate); // newTask(TurnOn.class, curveDate,
                                                                              // rsv.getRoom());
                        turnOn.setReservation(rsv);
                        thermodomoticService.execute(turnOn);
                        // addOrder(rsv.getRoom().getName(), ordersByRoomName, curveDate.getTime(), turnOn,
                        // TurnOn.class);

                    } else {
                        TurnOn turnOn = new TurnOn(rsv.getRoom(), taskStart); // thermodomoticService.newTask(TurnOn.class,
                                                                              // taskStart, rsv.getRoom());
                        turnOn.setReservation(rsv);
                        thermodomoticService.execute(turnOn);
                        // addOrder(rsv.getRoom().getName(), ordersByRoomName, taskStart.getTime(), turnOn,
                        // TurnOn.class);
                    }
                    // turn on all but heat X minutes before reservation starts
                    Date taskDate = rsv.getStart(); // DateUtils.addMinutes(rsv.getStart(), -startAllMinutesBefore);
                    TurnOnAllExceptHeat allOnButHeat = new TurnOnAllExceptHeat(rsv.getRoom(), taskDate); // .newTask(TurnOnAllExceptHeat.class,taskDate,
                                                                                                         // rsv.getRoom());
                    allOnButHeat.setReservation(rsv);
                    thermodomoticService.execute(allOnButHeat);
                    // addOrder(rsv.getRoom().getName(), ordersByRoomName, taskDate.getTime(), allOnButHeat,
                    // TurnOnAllExceptHeat.class);

                    // turn off all but heat X minutes after reservation ends
                    taskDate = rsv.getEnd(); // DateUtils.addMinutes(rsv.getEnd(), finishAllMinutesLater);
                    TurnOffAll turnOffAll = new TurnOffAll(rsv.getRoom(), taskDate); // thermodomoticService.newTask(TurnOffAll.class,
                                                                                     // taskDate, rsv.getRoom());

                    turnOffAll.setReservation(rsv);
                    thermodomoticService.execute(turnOffAll);
                    // addOrder(rsv.getRoom().getName(), ordersByRoomName, taskDate.getTime(), turnOffAll,
                    // TurnOffAll.class);
                }
            }

            // reviewOrders(ordersByRoomName);
            // executeOrders(thermodomoticService, ordersByRoomName);

        } catch (Exception e) {
            logger.error("Main task erminated with errors", e);
        }
        MainTask nextTask = new MainTask(); // BeanProvider.getContextualReference(MainTask.class, false);
        nextTask.setPriority(ExecutionPriority.NORMAL);
        nextTask.setExecution(DateUtils.addMinutes(getExecution(), checkEachMinutes));
        thermodomoticService.execute(nextTask);
        logger.info("Launched next main task");
    }

    private List<Reservation> mergeReservations(List<Reservation> reservations) {
        if (reservations == null || reservations.isEmpty())
            return reservations;

        Map<String, List<Reservation>> reservationsByRoom = new HashMap<String, List<Reservation>>();
        List<Reservation> result = new ArrayList<Reservation>();

        for (Reservation r : reservations)
            addReservation(reservationsByRoom, r);

        for (List<Reservation> reserv : reservationsByRoom.values()) {

            if (reserv.isEmpty())
                continue;
            Room room = reserv.get(0).getRoom();
            logger.info("Merging reservations of room: " + room.getName());
            List<Long> dates = new ArrayList<Long>(2 * reserv.size());
            for (Reservation r : reserv) {
                dates.add(r.getStart().getTime() - startAllMinutesBefore * MsInMinute);
                dates.add(-(r.getEnd().getTime() + finishAllMinutesLater * MsInMinute));
            }
            logger.info("Before sorting ");
            try {
                Collections.sort(dates, new Comparator<Long>() {
                    @Override
                    public int compare(Long o1, Long o2) {
                        long x = Math.abs(o1);
                        long y = Math.abs(o2);
                        return (x < y) ? -1 : ((x == y) ? 0 : 1);
                    }
                });
            } catch (Throwable e) {
                logger.error(">>>>>>>" + e.getMessage(), e);
            }

            List<Long> dates_filtered = new ArrayList<Long>(dates.size());
            for (int i = 0; i < dates.size() - 1; i++) {
                if (Math.abs(dates.get(i) + dates.get(i + 1)) > merge_tolerance_time) {
                    dates_filtered.add(dates.get(i));

                } else {
                    i++;
                }
            }
            dates_filtered.add(dates.get(dates.size() - 1)); // last one

            dates = dates_filtered;

            logger.info("Before starting");
            long start = dates.get(0);
            for (int cont = 0, i = 0; i < dates.size(); i++) {
                if (dates.get(i) > 0)
                    cont++;
                else
                    cont--;

                if (cont == 0) {
                    long end = dates.get(i);
                    logger.info("New reservation: " + new Date(Math.abs(start)).toString() + " - "
                            + new Date(Math.abs(end)).toString());
                    result.add(new Reservation(null, null, room, new Date(Math.abs(start)), new Date(Math.abs(end))));

                    // start for the next reservation
                    if (i < dates.size() - 1)
                        start = dates.get(i + 1);
                }

            }

        }

        logger.info("Final total reservations: " + result.size());

        return result;
    }

    private final void addReservation(Map<String, List<Reservation>> reservationsByRoomName, Reservation reservation) {
        if (reservation == null || reservation.getRoom() == null || reservation.getRoom().getName() == null)
            return;
        List<Reservation> reservations = reservationsByRoomName.get(reservation.getRoom().getName());
        if (reservations == null)
            reservationsByRoomName.put(reservation.getRoom().getName(), reservations = new ArrayList<Reservation>());
        reservations.add(reservation);
    }

    @Override
    public void run() {
        String previousThreadName = Thread.currentThread().getName();
        try {
            Thread.currentThread().setName(getThreadNameSuffix());
            logger.info("Running...");
            execute();
            logger.info("Terminated");
        } catch (Exception e) {
            logger.error("Terminated with errors", e);
        } finally {
            Thread.currentThread().setName(previousThreadName);
        }
    }

}
