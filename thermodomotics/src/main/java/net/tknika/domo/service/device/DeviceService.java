package net.tknika.domo.service.device;

import net.tknika.domo.model.DomoticData;
import net.tknika.domo.model.Room;

public interface DeviceService {

    DomoticData getRoomData(Room room);

    void turnOnHeating(Room room);

    void turnOffHeating(Room room);

    void turnOnAll(Room room);

    void turnOffAll(Room room);

    void turnOnAllExceptHeat(Room room);

    Double getOutsideTemperature();

}
