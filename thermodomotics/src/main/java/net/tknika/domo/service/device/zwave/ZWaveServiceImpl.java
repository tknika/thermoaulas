package net.tknika.domo.service.device.zwave;


//@Default
//@ZWaveService
//@Singleton
public class ZWaveServiceImpl /* implements DeviceService */{
    //
    // private static final Logger logger = Logger.getLogger(ZWaveServiceImpl.class);
    // private static final int MAX_RETRIES = 5;
    //
    // private static long userId;
    // private static String token;
    //
    // @Inject
    // @ConfigProperty(name = "zwave.gateway.ip")
    // private String gatewayIP;
    // @Inject
    // @ConfigProperty(name = "zwave.gateway.port")
    // private int gatewayPort;
    // @Inject
    // @ConfigProperty(name = "zwave.gateway.username")
    // private String username;
    // @Inject
    // @ConfigProperty(name = "zwave.gateway.password")
    // private String password;
    // @Inject
    // @ConfigProperty(name = "zwave.gateway.url.getToken")
    // private String getTokenUrl;
    // @Inject
    // @ConfigProperty(name = "zwave.gateway.url.heatOn")
    // private String heatOnUrl;
    // @Inject
    // @ConfigProperty(name = "zwave.gateway.url.heatOff")
    // private String heatOffUrl;
    // @Inject
    // @ConfigProperty(name = "zwave.gateway.url.getData")
    // private String getDataUrl;
    // @Inject
    // @ConfigProperty(name = "zwave.gateway.url.allOn")
    // private String allOnUrl;
    // @Inject
    // @ConfigProperty(name = "zwave.gateway.url.allOff")
    // private String allOffUrl;
    // @Inject
    // @ConfigProperty(name = "zwave.gateway.url.allOnButHeat")
    // private String allOnButHeat;
    //
    // @Override
    // public DomoticData getRoomData(Room room) {
    // logger.info("zwave - get room domotic data");
    // DomoticData data = null;
    // CloseableHttpClient httpclient = HttpClients.createDefault();
    // HttpPost httpPost = new HttpPost(MessageFormat.format(getDataUrl, room.getCode()));
    // try {
    // addUserTokenEntity(httpPost);
    // CloseableHttpResponse response = httpclient.execute(httpPost);
    // if (response.getStatusLine().getStatusCode() == 403) {
    // getToken();
    // }
    // httpPost = new HttpPost(MessageFormat.format(getDataUrl, room.getCode()));
    // addUserTokenEntity(httpPost);
    // int retry = 0;
    // do {
    // retry++;
    // response = httpclient.execute(httpPost);
    // } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);
    // JSONObject json = new JSONObject(EntityUtils.toString(response.getEntity()));
    // data = new DomoticData(new Date());
    // data.setTemperature(json.getJSONObject("items").getDouble("everspring_temperature"));
    // try {
    // data.setPresence(json.getJSONObject("items").getDouble("multisensor_sensor"));
    // } catch (JSONException e) {
    // logger.error("Parse error: " + json.getJSONObject("items").toString(), e);
    // }
    // } catch (ClientProtocolException e) {
    // logger.error("error getting domotic data", e);
    // } catch (IOException e) {
    // logger.error("error getting domotic data", e);
    // } catch (JSONException e) {
    // logger.error("error getting domotic data", e);
    // }
    // return data;
    // }
    //
    // @Override
    // public void turnOnHeating(Room room) {
    // logger.info("zwave - turn on heating");
    // CloseableHttpClient httpclient = HttpClients.createDefault();
    // HttpPost httpPost = new HttpPost(MessageFormat.format(heatOnUrl, room.getCode()));
    // CloseableHttpResponse response;
    // try {
    // int retry = 0;
    // do {
    // retry++;
    // addUserTokenEntity(httpPost);
    // response = httpclient.execute(httpPost);
    // if (response.getStatusLine().getStatusCode() == 403) {
    // getToken();
    // httpPost = new HttpPost(MessageFormat.format(heatOnUrl, room.getCode()));
    // addUserTokenEntity(httpPost);
    // response = httpclient.execute(httpPost);
    // }
    // } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);
    // logger.error("number of connections attempted: " + retry);
    // } catch (ClientProtocolException e) {
    // logger.error("error turning on heating", e);
    // } catch (IOException e) {
    // logger.error("error turning on heating", e);
    // }
    // }
    //
    // private void addUserTokenEntity(HttpPost httpPost) throws UnsupportedEncodingException {
    // List<NameValuePair> params = new ArrayList<NameValuePair>();
    // params.add(new BasicNameValuePair("user", String.valueOf(userId)));
    // params.add(new BasicNameValuePair("token", token));
    // httpPost.setEntity(new UrlEncodedFormEntity(params));
    // }
    //
    // @Override
    // public void turnOffHeating(Room room) {
    // logger.info("zwave - turn off heating");
    // CloseableHttpClient httpclient = HttpClients.createDefault();
    // HttpPost httpPost = new HttpPost(MessageFormat.format(heatOffUrl, room.getCode()));
    // CloseableHttpResponse response;
    // try {
    // int retry = 0;
    // do {
    // retry++;
    // addUserTokenEntity(httpPost);
    // response = httpclient.execute(httpPost);
    // if (response.getStatusLine().getStatusCode() == 403) {
    // getToken();
    // httpPost = new HttpPost(MessageFormat.format(heatOffUrl, room.getCode()));
    // addUserTokenEntity(httpPost);
    // response = httpclient.execute(httpPost);
    // }
    // } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);
    // logger.error("number of connections attempted: " + retry);
    // } catch (ClientProtocolException e) {
    // logger.error("error turning off heating", e);
    // } catch (IOException e) {
    // logger.error("error turning off heating", e);
    // }
    // }
    //
    // public void getToken() throws ClientProtocolException, IOException, JSONException {
    // CloseableHttpClient httpclient = HttpClients.createDefault();
    // HttpPost post = new HttpPost(getTokenUrl);
    // List<NameValuePair> params = new ArrayList<NameValuePair>();
    // params.add(new BasicNameValuePair("username", username));
    // params.add(new BasicNameValuePair("password", password));
    // post.setEntity(new UrlEncodedFormEntity(params));
    // CloseableHttpResponse response = httpclient.execute(post);
    // JSONObject json = new JSONObject(EntityUtils.toString(response.getEntity()));
    // if (json.has("token")) {
    // token = json.getString("token");
    // }
    // if (json.has("user")) {
    // userId = json.getLong("user");
    // }
    // httpclient.close();
    // }
    //
    // @Override
    // public void turnOnAll(Room room) {
    // logger.info("zwave - turnOnAll");
    // CloseableHttpClient httpclient = HttpClients.createDefault();
    // HttpPost httpPost = new HttpPost(MessageFormat.format(allOnUrl, room.getCode()));
    // CloseableHttpResponse response;
    // try {
    // int retry = 0;
    // do {
    // retry++;
    // addUserTokenEntity(httpPost);
    // response = httpclient.execute(httpPost);
    // if (response.getStatusLine().getStatusCode() == 403) {
    // getToken();
    // httpPost = new HttpPost(MessageFormat.format(allOnUrl, room.getCode()));
    // addUserTokenEntity(httpPost);
    // response = httpclient.execute(httpPost);
    // }
    // } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);
    // logger.error("number of connections attempted: " + retry);
    // } catch (ClientProtocolException e) {
    // logger.error("error turnOnAll", e);
    // } catch (IOException e) {
    // logger.error("error turnOnAll", e);
    // }
    //
    // }
    //
    // @Override
    // public void turnOffAll(Room room) {
    // logger.info("zwave - turnOffAll");
    // CloseableHttpClient httpclient = HttpClients.createDefault();
    // HttpPost httpPost = new HttpPost(MessageFormat.format(allOffUrl, room.getCode()));
    // CloseableHttpResponse response;
    // try {
    // int retry = 0;
    // do {
    // retry++;
    // addUserTokenEntity(httpPost);
    // response = httpclient.execute(httpPost);
    // if (response.getStatusLine().getStatusCode() == 403) {
    // getToken();
    // httpPost = new HttpPost(MessageFormat.format(allOffUrl, room.getCode()));
    // addUserTokenEntity(httpPost);
    // response = httpclient.execute(httpPost);
    // }
    // } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);
    // logger.error("number of connections attempted: " + retry);
    // } catch (ClientProtocolException e) {
    // logger.error("error turnOffAll", e);
    // } catch (IOException e) {
    // logger.error("error turnOffAll", e);
    // }
    // }
    //
    // @Override
    // public void turnOnAllExceptHeat(Room room) {
    // logger.info("zwave - turnOnAllExceptHeat");
    // CloseableHttpClient httpclient = HttpClients.createDefault();
    // HttpPost httpPost = new HttpPost(MessageFormat.format(allOnButHeat, room.getCode()));
    // CloseableHttpResponse response;
    // try {
    // int retry = 0;
    // do {
    // retry++;
    // addUserTokenEntity(httpPost);
    // response = httpclient.execute(httpPost);
    // if (response.getStatusLine().getStatusCode() == 403) {
    // getToken();
    // httpPost = new HttpPost(MessageFormat.format(allOnButHeat, room.getCode()));
    // addUserTokenEntity(httpPost);
    // response = httpclient.execute(httpPost);
    // }
    // } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);
    // logger.error("number of connections attempted: " + retry);
    // } catch (ClientProtocolException e) {
    // logger.error("error turnOnAllExceptHeat", e);
    // } catch (IOException e) {
    // logger.error("error turnOnAllExceptHeat", e);
    // }
    // }
    //
    //
    //
}
