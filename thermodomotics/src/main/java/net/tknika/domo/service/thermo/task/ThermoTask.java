package net.tknika.domo.service.thermo.task;

import static net.tknika.domo.model.ExecutionPriority.NORMAL;

import java.util.Date;

import net.tknika.domo.model.ExecutionPriority;
import net.tknika.domo.persistence.ThermodomoticsDAO;
import net.tknika.domo.persistence.impl.JPAThermodomoticsDAO;
import net.tknika.domo.service.device.DeviceService;
import net.tknika.domo.service.device.zwave.OpenhabServiceImpl;
import net.tknika.domo.service.reservation.ReservationService;
import net.tknika.domo.service.reservation.gcal.GoogleCalendarServiceImpl;
import net.tknika.domo.service.thermo.ThermodomoticService;

public abstract class ThermoTask implements Runnable {

    protected ThermodomoticsDAO dao;
    protected ThermodomoticService thermodomoticService;

    protected ReservationService reservationService;

    protected DeviceService deviceService;
    private Date execution;
    private ExecutionPriority priority;

    public ThermoTask() {
        init();
    }

    public ThermoTask(Date execution) {
        this(execution, NORMAL);
    }

    public ThermoTask(Date execution, ExecutionPriority priority) {
        this.execution = execution;
        this.priority = priority;
        init();
    }

    private void init() {
        thermodomoticService = ThermodomoticService.getInstance();
        deviceService = new OpenhabServiceImpl();
        reservationService = GoogleCalendarServiceImpl.getInstance();
        dao = JPAThermodomoticsDAO.getInstance();
    }

    public final Date getExecution() {
        return execution;
    }

    public final void setExecution(Date execution) {
        this.execution = execution;
    }

    public final ExecutionPriority getPriority() {
        return priority;
    }

    public final void setPriority(ExecutionPriority priority) {
        this.priority = priority;
    }

    protected abstract String getThreadNameSuffix();

    protected abstract void execute();

}
