package net.tknika.domo.service.classifier;

public abstract class ClassifierService {

    public abstract long getTimeToHeatOn(double initTemp, double finalTemp, double externalTemp);
}
