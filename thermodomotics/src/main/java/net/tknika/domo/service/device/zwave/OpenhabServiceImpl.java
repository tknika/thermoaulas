package net.tknika.domo.service.device.zwave;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.tknika.domo.Config;
import net.tknika.domo.model.DomoticData;
import net.tknika.domo.model.Room;
import net.tknika.domo.service.device.DeviceService;

import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class OpenhabServiceImpl implements DeviceService {

    private static final Logger logger = Logger.getLogger(OpenhabServiceImpl.class);
    private static final int MAX_RETRIES = 5;

    private static long userId;
    private static String token;

    private String gatewayIP;
    private int gatewayPort;
    private String username;
    private String password;
    private String getTokenUrl;
    private String heatOnUrl;
    private String heatOffUrl;
    private String getDataUrl;
    private String allOnUrl;
    private String allOffUrl;
    private String allOnButHeat;
    private String sensorTemperatureName;
    private String sensorPresenceName;
    private String outsideTemperatureUrl;

    public OpenhabServiceImpl() {

        try {
            Properties p = new Properties();

            if (getClass().getClassLoader().getResourceAsStream(Config.File) == null)
                logger.error("Config File Not Found");

            p.load(getClass().getClassLoader().getResourceAsStream(Config.File));

            gatewayIP = p.getProperty("openhab.gateway.ip");
            try {
                gatewayPort = Integer.parseInt(p.getProperty("openhab.gateway.port"));
            } catch (Exception e) {
            }
            username = p.getProperty("openhab.gateway.username");
            password = p.getProperty("openhab.gateway.password");

            getTokenUrl = p.getProperty("openhab.gateway.url.getToken");
            heatOnUrl = p.getProperty("openhab.gateway.url.heatOn");
            heatOffUrl = p.getProperty("openhab.gateway.url.heatOff");
            getDataUrl = p.getProperty("openhab.gateway.url.getData");
            allOnUrl = p.getProperty("openhab.gateway.url.allOn");
            allOffUrl = p.getProperty("openhab.gateway.url.allOff");
            allOnButHeat = p.getProperty("openhab.gateway.url.allOnButHeat");
            sensorTemperatureName = p.getProperty("openhab.temperature_sensor_name");
            sensorPresenceName = p.getProperty("openhab.presence_sensor_name");
            outsideTemperatureUrl = p.getProperty("openhab.outside_temperature");

        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public static void main(String[] args) {
        Room room = new Room();
        room.setName("Nafarroa Aretoa");
        room.setCode("nafarroa103");

        OpenhabServiceImpl deviceService = new OpenhabServiceImpl();
        Double temp = deviceService.getOutsideTemperature();
        // CentralHeating.isOn(temp);

        // DomoticData data = deviceService.getRoomData(room);
        // deviceService.turnOnHeating(room);
        // deviceService.turnOffHeating(room);
        // deviceService.turnOffAll(room);
        // deviceService.turnOnAllExceptHeat(room);

    }

    @Override
    public DomoticData getRoomData(Room room) {
        logger.info("openhab - get room domotic data");
        DomoticData data = null;
        // CloseableHttpClient httpclient = HttpClients.createDefault();
        // HttpClient httpClient = new HttpClient(MessageFormat.format(getDataUrl, room.getId()));

        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));
        CloseableHttpClient httpClient =
                HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();

        try {

            HttpGet httpGet = new HttpGet(getDataUrl);
            CloseableHttpResponse response;
            int retry = 0;
            do {
                retry++;
                response = httpClient.execute(httpGet);
            } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = null;
            try {
                db = dbFactory.newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(EntityUtils.toString(response.getEntity())));
                try {
                    Document doc = db.parse(is);

                    NodeList list = doc.getElementsByTagName("item");
                    if (list != null) {
                        data = new DomoticData(new Date());
                        for (int i = 0; i < list.getLength(); i++) {
                            Node n = list.item(i);
                            if (n.getTextContent().contains(room.getCode() + sensorTemperatureName)) {
                                NodeList nl = n.getChildNodes();
                                for (int j = 0; j < nl.getLength(); j++) {
                                    if (nl.item(j).getNodeName().equalsIgnoreCase("state")) {
                                        try {
                                            data.setTemperature(Double.parseDouble(nl.item(j).getNodeValue()));
                                            break;
                                        } catch (Exception e) {
                                            logger.error(e.getMessage());
                                        }
                                    }
                                }

                            } else if (n.getTextContent().contains(room.getCode() + sensorPresenceName)) {
                                NodeList nl = n.getChildNodes();
                                for (int j = 0; j < nl.getLength(); j++) {
                                    if (nl.item(j).getNodeName().equalsIgnoreCase("state")) {
                                        try {
                                            data.setPresence(nl.item(j).getNodeValue().equalsIgnoreCase("OPEN"));
                                            break;
                                        } catch (Exception e) {
                                            logger.error(e.getMessage());
                                        }
                                    }
                                }
                            }
                        }
                    }

                } catch (SAXException e) {
                    logger.error(e.getMessage());
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            } catch (ParserConfigurationException e1) {
                logger.error(e1.getMessage());
            }

        } catch (ClientProtocolException e) {
            logger.error("error getting domotic data", e);
        } catch (IOException e) {
            logger.error("error getting domotic data", e);
        } catch (JSONException e) {
            logger.error("error getting domotic data", e);
        }
        return data;
    }

    @Override
    public void turnOnHeating(Room room) {
        logger.info("openhab - turn on heating");

        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));
        CloseableHttpClient httpClient =
                HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();

        try {

            HttpPut httpPost = new HttpPut(MessageFormat.format(heatOnUrl, room.getCode()));
            httpPost.addHeader("Content-Type", "text/plain");

            httpPost.setEntity(new StringEntity("ON"));
            CloseableHttpResponse response;
            int retry = 0;
            do {
                retry++;
                response = httpClient.execute(httpPost);
            } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);

        } catch (ClientProtocolException e) {
            logger.error("error turning on heating", e);
        } catch (IOException e) {
            logger.error("error turning on heating", e);
        }

    }

    private void addUserTokenEntity(HttpPost httpPost) throws UnsupportedEncodingException {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", String.valueOf(userId)));
        params.add(new BasicNameValuePair("token", token));
        httpPost.setEntity(new UrlEncodedFormEntity(params));
    }

    private void addParam(HttpPost httpPost, String name, String value) throws UnsupportedEncodingException {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair(name, value));
        httpPost.setEntity(new UrlEncodedFormEntity(params));
    }

    @Override
    public void turnOffHeating(Room room) {
        logger.info("openhab - turn off heating");
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));
        CloseableHttpClient httpClient =
                HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();

        try {

            HttpPut httpPost = new HttpPut(MessageFormat.format(heatOffUrl, room.getCode()));
            httpPost.addHeader("Content-Type", "text/plain");

            httpPost.setEntity(new StringEntity("OFF"));
            CloseableHttpResponse response;
            int retry = 0;
            do {
                retry++;
                response = httpClient.execute(httpPost);
            } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);
        } catch (ClientProtocolException e) {
            logger.error("error turning off heating", e);
        } catch (IOException e) {
            logger.error("error turning off heating", e);
        }
    }

    public void getToken() throws ClientProtocolException, IOException, JSONException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost post = new HttpPost(getTokenUrl);
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("username", username));
        params.add(new BasicNameValuePair("password", password));
        post.setEntity(new UrlEncodedFormEntity(params));
        CloseableHttpResponse response = httpclient.execute(post);
        JSONObject json = new JSONObject(EntityUtils.toString(response.getEntity()));
        if (json.has("token")) {
            token = json.getString("token");
        }
        if (json.has("user")) {
            userId = json.getLong("user");
        }
        httpclient.close();
    }

    @Override
    public void turnOnAll(Room room) {
        logger.info("openhab - turnOnAll");
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));
        CloseableHttpClient httpClient =
                HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();

        try {

            HttpPut httpPost = new HttpPut(MessageFormat.format(allOnUrl, room.getCode()));
            httpPost.addHeader("Content-Type", "text/plain");

            httpPost.setEntity(new StringEntity("ON"));
            CloseableHttpResponse response;
            int retry = 0;
            do {
                retry++;
                response = httpClient.execute(httpPost);
            } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);
        } catch (ClientProtocolException e) {
            logger.error("error turnOnAll", e);
        } catch (IOException e) {
            logger.error("error turnOnAll", e);
        }

    }

    @Override
    public void turnOffAll(Room room) {
        logger.info("openhab - turnOffAll");
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));
        CloseableHttpClient httpClient =
                HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();

        try {

            HttpPut httpPost = new HttpPut(MessageFormat.format(allOffUrl, room.getCode()));
            httpPost.addHeader("Content-Type", "text/plain");

            httpPost.setEntity(new StringEntity("OFF"));
            CloseableHttpResponse response;
            int retry = 0;
            do {
                retry++;
                response = httpClient.execute(httpPost);
            } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);
        } catch (ClientProtocolException e) {
            logger.error("error turnOffAll", e);
        } catch (IOException e) {
            logger.error("error turnOffAll", e);
        }
    }

    @Override
    public void turnOnAllExceptHeat(Room room) {
        logger.info("openhab - turnOnAllExceptHeat");
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));
        CloseableHttpClient httpClient =
                HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();

        try {

            HttpPut httpPost = new HttpPut(MessageFormat.format(allOnButHeat, room.getCode()));
            httpPost.addHeader("Content-Type", "text/plain");

            httpPost.setEntity(new StringEntity("ON"));
            CloseableHttpResponse response;
            int retry = 0;
            do {
                retry++;
                response = httpClient.execute(httpPost);
            } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);
        } catch (ClientProtocolException e) {
            logger.error("error turnOnAllExceptHeat", e);
        } catch (IOException e) {
            logger.error("error turnOnAllExceptHeat", e);
        }
    }

    @Override
    public Double getOutsideTemperature() {
        logger.info("openhab - get Outside Temperature");

        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));
        CloseableHttpClient httpClient =
                HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();

        try {

            HttpGet httpGet = new HttpGet(outsideTemperatureUrl);
            CloseableHttpResponse response;
            int retry = 0;
            do {
                retry++;
                response = httpClient.execute(httpGet);
            } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = null;
            try {
                db = dbFactory.newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(EntityUtils.toString(response.getEntity())));

                try {
                    Document doc = db.parse(is);

                    NodeList list = doc.getElementsByTagName("item");
                    if (list != null) {

                        for (int i = 0; i < list.getLength(); i++) {
                            Node n = list.item(i);

                            NodeList nl = n.getChildNodes();
                            for (int j = 0; j < nl.getLength(); j++) {
                                if (nl.item(j).getNodeName().equalsIgnoreCase("state")) {
                                    try {
                                        return Double.parseDouble(nl.item(j).getFirstChild().getNodeValue());
                                    } catch (Exception e) {

                                    }
                                }
                            }

                        }
                    }

                }

                catch (SAXException e) {
                    logger.error(e.getMessage());
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            } catch (ParserConfigurationException e1) {
                logger.error(e1.getMessage());
            }

        } catch (ClientProtocolException e) {
            logger.error("error getting Outside Temperature", e);
        } catch (IOException e) {
            logger.error("error getting Outside Temperature", e);
        } catch (JSONException e) {
            logger.error("error getting Outside Temperature", e);
        }
        return null;
    }

}
