package net.tknika.domo.service.classifier.rtree;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import javax.enterprise.inject.Alternative;
import javax.inject.Singleton;

import net.tknika.domo.Config;
import net.tknika.domo.model.Room;
import net.tknika.domo.model.TrainingData;
import net.tknika.domo.persistence.ThermodomoticsDAO;
import net.tknika.domo.persistence.impl.JPAThermodomoticsDAO;
import net.tknika.domo.service.classifier.ClassifierService;
import net.tknika.domo.service.thermo.RandomForest;

import org.apache.log4j.Logger;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

@Alternative
@RandomTreeService
@Singleton
public class RandomTreeServiceImpl extends ClassifierService {

    private static final Logger logger = Logger.getLogger(RandomTreeServiceImpl.class);
    private final static double WEIGHT = 1;

    ThermodomoticsDAO dao;
    private Room room;

    int maxDepth;
    int numTrees;
    int numFeatures;

    public RandomTreeServiceImpl(Room room) {
        this();
        this.room = room;
    }

    public RandomTreeServiceImpl() {
        dao = JPAThermodomoticsDAO.getInstance();
        Properties p = new Properties();
        try {
            p.load(getClass().getClassLoader().getResourceAsStream(Config.File));
            try {
                maxDepth = Integer.parseInt(p.getProperty("thermo.train.rf.maxDepth"));
            } catch (Exception e) {
            }
            try {
                numTrees = Integer.parseInt(p.getProperty("thermo.train.rf.numTrees"));
            } catch (Exception e) {
            }
            try {
                numFeatures = Integer.parseInt(p.getProperty("thermo.train.rf.numFeatures"));
            } catch (Exception e) {
            }

        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public long getTimeToHeatOn(double initTemp, double finalTemp, double externalTemp) {
        // Create classifier and attributes
        RandomForest randomForest = new RandomForest();
        ArrayList<Attribute> atts = new ArrayList<Attribute>(4);
        Attribute startTemp = new Attribute("startTemp");
        Attribute endTemp = new Attribute("endTemp");
        Attribute attExternalTemp = new Attribute("externalTemp");
        Attribute timeSpent = new Attribute("timeSpent");
        atts.add(startTemp);
        atts.add(endTemp);
        atts.add(attExternalTemp);
        atts.add(timeSpent);

        // create instances to train random forest
        Instances data = new Instances("TestInstances", atts, dao.getTrainingData(room).size());
        data.setClassIndex(3);
        for (TrainingData td : dao.getTrainingData(room)) {
            Instance instance = new DenseInstance(4);
            instance.setValue(startTemp, td.getInitialTemperature());
            instance.setValue(endTemp, td.getFinalTemperature());
            instance.setValue(attExternalTemp, td.getExternalTemperature());
            instance.setValue(timeSpent, td.getTimeSpentInMinutes());
            data.add(instance);
        }
        randomForest.setMaxDepth(maxDepth);
        randomForest.setNumFeatures(numFeatures);
        randomForest.setNumTrees(numTrees);

        try {
            randomForest.buildClassifier(data);
            // Create new instance from received data
            double[] attributes = new double[] { initTemp, finalTemp, externalTemp, 0 };
            Instance instance = new DenseInstance(WEIGHT, attributes);
            return (long) randomForest.classifyInstance(instance);
        } catch (Exception e) {
            logger.error("error with classifier", e);
            return 0;
        }
    }
}
