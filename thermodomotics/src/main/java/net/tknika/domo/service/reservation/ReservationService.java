package net.tknika.domo.service.reservation;

import java.util.List;

import net.tknika.domo.model.Reservation;
import net.tknika.domo.model.ReservationFilter;
import net.tknika.domo.model.Room;

public interface ReservationService {

    List<Reservation> getReservations();

    void deleteReservations();

    List<Reservation> getReservations(ReservationFilter filter);

    List<Reservation> getReservations(Room room);

    Reservation getReservation(String roomId, long date);

    void makeReservation(Reservation reservation);

    void deleteReservation(String reservationId);

}
