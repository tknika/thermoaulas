package net.tknika.domo.service.meteo;

import java.util.Date;

import net.tknika.domo.model.MeteoData;

public interface MeteoService {

    MeteoData getObservation(Date datetime);

    MeteoData getForecast(Date datetime);

}
