package net.tknika.domo.service.thermo.task;

import java.util.Date;

import net.tknika.domo.model.ExecutionPriority;
import net.tknika.domo.model.Reservation;
import net.tknika.domo.model.Room;
import net.tknika.domo.model.TrainingData;
import net.tknika.domo.service.device.DeviceService;
import net.tknika.domo.service.device.zwave.OpenhabServiceImpl;

import org.apache.log4j.Logger;

public class TurnOffHeating extends RoomTask {

    private static final String THREAD_NAME_SUFFIX = "heat-off";
    private static final Logger logger = Logger.getLogger(TurnOffHeating.class);

    private Reservation reservation;
    private long trainingDataStartTime;
    /**
     * To prevent system from submit this case to the training data repository
     */
    private boolean notNormalTermination;

    public TurnOffHeating() {
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public long getTrainingDataStartTime() {
        return trainingDataStartTime;
    }

    public void setTrainingDataStartTime(long trainingDataStartTime) {
        this.trainingDataStartTime = trainingDataStartTime;
    }

    public TurnOffHeating(Room room, Date execution, ExecutionPriority priority, Reservation reservation,
            long tdStartTime) {
        super(room, execution, priority);
        this.reservation = reservation;
        this.trainingDataStartTime = tdStartTime;
    }

    public TurnOffHeating(Room room, Date execution, Reservation reservation, long tdStartTime) {
        super(room, execution);
        this.reservation = reservation;
        this.trainingDataStartTime = tdStartTime;
    }

    public TurnOffHeating(Reservation reservation, long tdStartTime) {
        super(reservation.getRoom());
        this.reservation = reservation;
        this.trainingDataStartTime = tdStartTime;
    }

    public TurnOffHeating(Room room, Date date, ExecutionPriority priority) {
        super(room, date, priority);
    }

    public TurnOffHeating(Room room, Date date) {
        super(room, date);
    }

    @Override
    protected String getThreadNameSuffix() {
        return THREAD_NAME_SUFFIX;
    }

    public final boolean isNotNormalTermination() {
        return notNormalTermination;
    }

    public final void setNotNormalTermination(boolean notNormalTermination) {
        this.notNormalTermination = notNormalTermination;
    }

    @Override
    public void execute() {
        logger.info("executing turn off heat");
        try {
            DeviceService deviceService = new OpenhabServiceImpl();// BeanProvider.getContextualReference(DeviceService.class);
            deviceService.turnOffHeating(getRoom());
            if (!notNormalTermination) {
                TrainingData data = dao.getTrainingData(getRoom(), new Date(trainingDataStartTime));
                if (data != null) {
                    logger.info("trainingdata not null");
                    data.setEnd(new Date().getTime());
                    data.setFinalTemperature(deviceService.getRoomData(getRoom()).getTemperature());
                    data.setElapsedTime(data.getEnd() - data.getStart());
                    logger.info("elapsed time: " + data.getElapsedTime());
                    logger.info("Time spent in minutes: " + data.getTimeSpentInMinutes());
                    dao.update(data);
                    logger.info("updated td for reservation " + reservation.getStart());
                    logger.info(data.toString());
                } else {
                    logger.info("trainingdata is null");
                }

            }
        } catch (Exception e) {
            logger.error("terminated turn off heating with error", e);
        }
    }
}
