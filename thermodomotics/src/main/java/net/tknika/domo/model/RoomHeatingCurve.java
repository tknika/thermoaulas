package net.tknika.domo.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * 
 * Celsius degrees vs time in seconds
 * 
 * @see http://www.davdata.nl/math/expfitting.html
 * 
 * @author acarrascal (acarrascal@daiasolutions.com)
 * 
 */
public class RoomHeatingCurve {

    private static final Logger logger = Logger.getLogger(RoomHeatingCurve.class);

    private static final int minRegrVectorSize = 3;

    // T = a exp(bt) + c
    // c is equilibrium temperature
    // b heating slope
    // a is initial temperature - equilibrium temperature
    private final double a, b, c;

    /**
     * 
     * @param a
     *            initial temperature - equilibrium temperature
     * @param b
     *            heating slope
     * @param c
     *            equilibrium temperature
     */
    public RoomHeatingCurve(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public static RoomHeatingCurve getInstance(List<Date> dates, List<Double> temperatures) {

        if (dates == null || temperatures == null || dates.size() < minRegrVectorSize
                || temperatures.size() < minRegrVectorSize)
            return null;

        int sampleSize = Math.min(dates.size(), temperatures.size());

        double[] t = new double[sampleSize];
        double[] T = new double[sampleSize];
        double[] difT = new double[sampleSize - 1];
        double[] dift = new double[sampleSize - 1];
        double[] derivT = new double[sampleSize - 1];
        double a = 0, b = 0, c = 0;

        for (int i = 0; i < sampleSize; i++) {
            t[i] = (dates.get(i).getTime() - dates.get(0).getTime()) / 10E3;
            T[i] = temperatures.get(i);
            if (i > 0) {
                difT[i - 1] = T[i] - T[i - 1];
                dift[i - 1] = t[i] - t[i - 1];
                derivT[i - 1] = difT[i - 1] / dift[i - 1];
            }
        }

        // T = a exp(bt) + c
        // b calculation
        for (int i = 1; i < sampleSize - 1; i++)
            b += (Math.log(derivT[i]) - Math.log(derivT[i - 1])) / dift[i - 1];
        b /= sampleSize - 2;

        // a calculation
        for (int i = 1; i < sampleSize; i++)
            a += difT[i - 1] / (Math.exp(b * t[i]) - Math.exp(b * t[i - 1]));
        a /= sampleSize - 1;

        // c calculation
        for (int i = 0; i < sampleSize; i++)
            c += T[i] - a * Math.exp(b * t[i]);
        c /= sampleSize;

        RoomHeatingCurve curve = new RoomHeatingCurve(a, b, c);
        return curve;

    }

    /**
     * 
     * @param startTemp
     * @param endTemp
     * @param elapsedTime
     *            in seconds
     * @return temperature after elapsedTime seconds
     */
    public double getTemperature(double startTemp, long elapsedTime) {

        double time = getTime(startTemp);
        return getTemperature(time + elapsedTime);

    }

    /**
     * 
     * @param temperature
     * @return elapsed time time in s
     */
    private final double getTime(double temperature) {
        return (Math.log((temperature - c) / a)) / b;
    }

    private final double getTemperature(double elapsedTime) {
        return a * Math.exp(b * elapsedTime) + c;
    }

    /**
     * 
     * @param reservationDate
     * @param startTemp
     * @param endTemp
     * 
     * @return long when the heating system has to be turned on. 0 if it is not needed.
     */
    public long getAnticipationHeatingTime(long reservationDate, double startTemp, double endTemp) {

        if (startTemp >= endTemp)
            return 0;

        double time = getTime(endTemp);
        double timeDiff = reservationDate - time * 10E3; // in ms

        double TurnOnTime = getTime(startTemp);
        return (long) (TurnOnTime * 10E3 + timeDiff);

    }

    public static void main(String args[]) {

        Date d = new Date();

        List<Date> dates = new ArrayList<Date>();
        List<Double> temps = new ArrayList<Double>();

        for (int i = 1; i <= 24; i++) {
            dates.add(new Date(d.getTime() + i * 300 * 1000l));
            temps.add(20 - 15 * Math.exp(-0.0005 * (i * 300)));

            logger.debug(20 - 15 * Math.exp(-0.0005 * (i * 300)));

        }
        RoomHeatingCurve rhc = RoomHeatingCurve.getInstance(dates, temps);

        long date = rhc.getAnticipationHeatingTime(d.getTime() + 2 * 60 * 60 * 1000, 7, 19);

        for (int i = 1; i <= 24; i++)
            logger.debug("" + rhc.getTemperature(10, i * 300));

    }

}
