package net.tknika.domo.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({
        @NamedQuery(name = "Room.findAll", query = "select r from Room r"),
        @NamedQuery(name = "Room.deleteAll", query = "delete from Room"),
        @NamedQuery(name = "Room.findName", query = "select r from Room r where r.name = :name")
})
public class Room {

    @Id
    // private String id;
    private String name;
    /**
     * Short name for references purposes
     */
    private String code;

    private String calendarId;

    @Temporal(TemporalType.DATE)
    private Date nextHeatingOn;
    @Temporal(TemporalType.DATE)
    private Date lastHeatingOn;
    @Temporal(TemporalType.DATE)
    private Date nextHeatingOff;
    @Temporal(TemporalType.DATE)
    private Date lastHeatingOff;
    @Temporal(TemporalType.DATE)
    private Date nextTraining;
    @Temporal(TemporalType.DATE)
    private Date lastTraining;

    //
    // public String getId() {
    // return id;
    // }
    //
    // public void setId(String id) {
    // this.id = id;
    // }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getNextHeatingOn() {
        return nextHeatingOn;
    }

    public void setNextHeatingOn(Date nextHeatingOn) {
        this.nextHeatingOn = nextHeatingOn;
    }

    public Date getLastHeatingOn() {
        return lastHeatingOn;
    }

    public void setLastHeatingOn(Date lastHeatingOn) {
        this.lastHeatingOn = lastHeatingOn;
    }

    public Date getNextHeatingOff() {
        return nextHeatingOff;
    }

    public void setNextHeatingOff(Date nextHeatingOff) {
        this.nextHeatingOff = nextHeatingOff;
    }

    public Date getLastHeatingOff() {
        return lastHeatingOff;
    }

    public void setLastHeatingOff(Date lastHeatingOff) {
        this.lastHeatingOff = lastHeatingOff;
    }

    public Date getNextTraining() {
        return nextTraining;
    }

    public void setNextTraining(Date nextTraining) {
        this.nextTraining = nextTraining;
    }

    public Date getLastTraining() {
        return lastTraining;
    }

    public void setLastTraining(Date lastTraining) {
        this.lastTraining = lastTraining;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Room other = (Room) obj;
        // if (id == null) {
        // if (other.id != null)
        // return false;
        // } else if (!id.equals(other.id))
        // return false;
        if (lastHeatingOff == null) {
            if (other.lastHeatingOff != null)
                return false;
        } else if (!lastHeatingOff.equals(other.lastHeatingOff))
            return false;
        if (lastHeatingOn == null) {
            if (other.lastHeatingOn != null)
                return false;
        } else if (!lastHeatingOn.equals(other.lastHeatingOn))
            return false;
        if (lastTraining == null) {
            if (other.lastTraining != null)
                return false;
        } else if (!lastTraining.equals(other.lastTraining))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (nextHeatingOff == null) {
            if (other.nextHeatingOff != null)
                return false;
        } else if (!nextHeatingOff.equals(other.nextHeatingOff))
            return false;
        if (nextHeatingOn == null) {
            if (other.nextHeatingOn != null)
                return false;
        } else if (!nextHeatingOn.equals(other.nextHeatingOn))
            return false;
        if (nextTraining == null) {
            if (other.nextTraining != null)
                return false;
        } else if (!nextTraining.equals(other.nextTraining))
            return false;
        return true;
    }

    public final String getCode() {
        return code;
    }

    public final void setCode(String code) {
        this.code = code;
    }

    public final String getCalendarId() {
        return calendarId;
    }

    public final void setCalendarId(String calendarId) {
        this.calendarId = calendarId;
    }

}
