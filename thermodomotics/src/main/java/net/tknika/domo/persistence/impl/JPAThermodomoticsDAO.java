package net.tknika.domo.persistence.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import net.tknika.domo.model.ReservationDates;
import net.tknika.domo.model.Room;
import net.tknika.domo.model.ThermoCurve;
import net.tknika.domo.model.TrainingData;
import net.tknika.domo.model.TrainingDataId;
import net.tknika.domo.persistence.ThermodomoticsDAO;

import org.apache.deltaspike.jpa.api.transaction.Transactional;

public class JPAThermodomoticsDAO implements ThermodomoticsDAO {

    private static final String persistence_unit_name = "thermoaulas";

    private EntityManagerFactory emf;

    private static JPAThermodomoticsDAO instance;

    public static ThermodomoticsDAO getInstance() {
        if (instance == null) {
            instance = new JPAThermodomoticsDAO();
        }
        if (!instance.emf.isOpen()) {
            instance.emf = Persistence.createEntityManagerFactory(persistence_unit_name);
        }
        return instance;
    }

    public JPAThermodomoticsDAO() {
        emf = Persistence.createEntityManagerFactory(persistence_unit_name);
    }

    public void close() {
        if (emf.isOpen()) {
            emf.close();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Room> getRooms() {
        EntityManager em = emf.createEntityManager();
        return em.createNamedQuery("Room.findAll").getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Room> getRooms(String roomName) {
        EntityManager em = emf.createEntityManager();
        return em.createNamedQuery("Room.findName").setParameter("name", roomName).getResultList();
    }

    @Override
    public Room getRoom(String id) {
        EntityManager em = emf.createEntityManager();
        return em.find(Room.class, id);
    }

    @Transactional
    @Override
    public void save(Room room) {
        EntityManager em = emf.createEntityManager();
        em.persist(room);
    }

    @Transactional
    @Override
    public void deleteRooms() {
        EntityManager em = emf.createEntityManager();
        em.createNamedQuery("ThermoCurve.deleteAll").executeUpdate();
        em.createNamedQuery("TrainingData.deleteAll").executeUpdate();
        em.createNamedQuery("Room.deleteAll").executeUpdate();
    }

    @Transactional
    @Override
    public void deleteRoom(String id) {
        EntityManager em = emf.createEntityManager();
        Room room = em.find(Room.class, id);
        em.remove(room);
    }

    @Override
    public TrainingData getTrainingData(Room room, Date start) {
        TrainingDataId id = new TrainingDataId();
        id.setId(room.getName());
        id.setStart(start.getTime());
        EntityManager em = emf.createEntityManager();
        return em.find(TrainingData.class, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TrainingData> getTrainingData(Room room, Date from, Date to) {
        EntityManager em = emf.createEntityManager();
        return em.createNamedQuery("TrainingData.findFromTo").setParameter("room", room).
                setParameter("start", from).setParameter("end", to).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TrainingData> getTrainingData(Room room) {
        EntityManager em = emf.createEntityManager();
        Query query = em.createNamedQuery("TrainingData.findAllForRoom");
        query.setParameter("room", room);
        return query.getResultList();
    }

    @Transactional
    @Override
    public void save(TrainingData trainingData) {
        EntityManager em = emf.createEntityManager();
        em.persist(trainingData);
    }

    @Transactional
    @Override
    public TrainingData update(TrainingData trainingData) {
        EntityManager em = emf.createEntityManager();
        return em.merge(trainingData);
    }

    @Override
    public void deleteTrainingDatas(Room room) {
        EntityManager em = emf.createEntityManager();
        em.createNamedQuery("TrainingData.deleteRoomsData").setParameter("room", room).executeUpdate();
    }

    @Transactional
    @Override
    public void deleteTrainingDatas() {
        EntityManager em = emf.createEntityManager();
        em.createNamedQuery("TrainingData.deleteAll").executeUpdate();
    }

    @Override
    public ThermoCurve getCurve(Room room) {
        EntityManager em = emf.createEntityManager();
        return em.find(ThermoCurve.class, room.getName());
    }

    @Transactional
    @Override
    public void save(ThermoCurve curve) {
        EntityManager em = emf.createEntityManager();
        em.persist(curve);
    }

    @Transactional
    @Override
    public ThermoCurve update(ThermoCurve curve) {
        EntityManager em = emf.createEntityManager();
        return em.merge(curve);
    }

    @Transactional
    @Override
    public void deleteCurves() {
        EntityManager em = emf.createEntityManager();
        em.createNamedQuery("ThermoCurve.deleteAll").executeUpdate();
    }

    @SuppressWarnings("unchecked")
    @Override
    public ReservationDates getReservationDates() {
        EntityManager em = emf.createEntityManager();
        List<ReservationDates> list = em.createNamedQuery("ReservationDates.find").getResultList();
        if (list == null || list.isEmpty()) {
            return null;
        } else {
            return list.get(0);
        }

    }

    @Transactional
    @Override
    public void save(ReservationDates rDates) {
        EntityManager em = emf.createEntityManager();
        em.persist(rDates);
    }

    @Transactional
    @Override
    public ReservationDates update(ReservationDates rDates) {
        EntityManager em = emf.createEntityManager();
        return em.merge(rDates);
    }

    @Transactional
    @Override
    public void deleteReservationDates() {
        EntityManager em = emf.createEntityManager();
        em.createNamedQuery("ReservationDates.deleteAll").executeUpdate();
    }

}
