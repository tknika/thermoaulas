package net.tknika.domo.service.reservation;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import net.tknika.domo.model.Reservation;
import net.tknika.domo.model.ReservationDates;
import net.tknika.domo.model.ReservationFilter;
import net.tknika.domo.persistence.ThermodomoticsDAO;
import net.tknika.domo.service.reservation.gcal.GoogleCalendarService;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(CdiTestRunner.class)
public class ReservationServiceTest {

    private static final Logger logger = Logger.getLogger(ReservationServiceTest.class);

    @Inject
    @GoogleCalendarService
    private ReservationService service;

    @Inject
    private ThermodomoticsDAO dao;

    @Test
    public void testReservationServices() {
        logger.info(service.getClass().getSimpleName());
        List<Reservation> reservations = service.getReservations();
        assertNotNull(reservations);
    }

    @Test
    public void getReservationsWithFilter() {
        dao.deleteReservationDates();
        ReservationFilter filter = new ReservationFilter();
        Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        Date tomorrow = DateUtils.addDays(today, 15);
        tomorrow = DateUtils.addSeconds(tomorrow, -1);
        filter.setFrom(today);
        filter.setTo(tomorrow);
        filter.setRoomId("zwave1");
        List<Reservation> reservations = service.getReservations(filter);
        if (reservations != null && !reservations.isEmpty()) {
            // create array of dates
            ReservationDates rDates = new ReservationDates();
            for (Reservation rsv : reservations) {
                rDates.addDates(rsv);
            }
            dao.save(rDates);
            ReservationDates storedDates = dao.getReservationDates();
            assertNotNull(storedDates);
            assertTrue(storedDates.equals(rDates.getDates()));
        }
    }

}
