package net.tknika.ai;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import javax.inject.Inject;

import net.tknika.domo.model.Room;
import net.tknika.domo.model.TrainingData;
import net.tknika.domo.persistence.TestData;
import net.tknika.domo.persistence.ThermodomoticsDAO;
import net.tknika.domo.service.thermo.RandomForest;
import net.tknika.domo.service.thermo.RoomTest;

import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import weka.classifiers.evaluation.Evaluation;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;

@RunWith(CdiTestRunner.class)
public class RandomForestTraining {

    private final static String CLASSIFIER_FILE = "randomForest.serialized";

    private Logger logger = Logger.getLogger(RandomForestTraining.class);

    @Inject
    ThermodomoticsDAO dao;

    @Inject
    private TestData testData;

    @Before
    public void loadTestData() {
        testData.load();
    }

    @SuppressWarnings("deprecation")
    @Test
    public void trainCPUWithVendorTest() {
        try {
            RandomForest randomForest = new RandomForest();
            BufferedReader reader = new BufferedReader(
                    new FileReader(RoomTest.class.getClassLoader().getResource("./data/cpu.with.vendor.arff").getFile()));
            Instances data = new Instances(reader);
            data.setClassIndex(7);
            randomForest.setDebug(true);
            randomForest.buildClassifier(data);
            logger.debug(data.size());
            // Evaluation evaluation = new Evaluation(data);
            // logger.debug("Error: " + evaluation.confusionMatrix());
            for (int i = 0; i < data.size(); i++) {
                logger.debug("result for instance " + i + "("
                        + data.get(i).toString().split(",")[data.numAttributes() - 1] + "): "
                        + randomForest.classifyInstance(data.get(i)));
            }
        } catch (Exception e) {
            logger.log(Priority.ERROR, e);
        }
    }

    @Test
    public void trainWithCSVData() {
        int bestMaxDepth = 0, bestNumTrees = 0, bestNumFeatures = 0;
        double error = Double.MAX_VALUE;
        try {
            Room room = dao.getRoom(dao.getRooms().get(0).getName());

            // Create random forest regression
            RandomForest randomForest = new RandomForest();
            ArrayList<Attribute> atts = new ArrayList<Attribute>();
            Attribute startTemp = new Attribute("startTemp");
            Attribute endTemp = new Attribute("endTemp");
            Attribute externalTemp = new Attribute("externalTemp");
            Attribute timeSpent = new Attribute("timeSpent");
            atts.add(startTemp);
            atts.add(endTemp);
            atts.add(externalTemp);
            atts.add(timeSpent);

            // Read data
            Instances data = new Instances("TestInstances", atts, dao.getTrainingData(room).size());
            data.setClassIndex(3);
            for (TrainingData td : dao.getTrainingData(room)) {
                Instance instance = new DenseInstance(4);
                instance.setValue(startTemp, td.getInitialTemperature());
                instance.setValue(endTemp, td.getFinalTemperature());
                instance.setValue(externalTemp, td.getExternalTemperature());
                instance.setValue(timeSpent, td.getTimeSpentInMinutes());
                data.add(instance);
            }

            // prepare benchmark values
            int[] maxDepthValues = new int[] { 5, 10, 20, 40, 50, 75, 100, 150 };
            int[] numTreesValues = new int[] { 200, 300, 400, 600, 800, 1000 };
            int[] numFeaturesValues = new int[] { 1, 2, 3, 4, 5, 6, 10 };

            // train with different benchmarks

            for (int i = 0; i < maxDepthValues.length - 1; i++) {
                randomForest.setMaxDepth(maxDepthValues[i]);
                for (int j = 0; j < numTreesValues.length - 1; j++) {
                    randomForest.setNumTrees(numTreesValues[j]);
                    for (int h = 0; h < numFeaturesValues.length - 1; h++) {
                        logger.debug("training with values:");
                        logger.debug("max depth=" + maxDepthValues[i]);
                        logger.debug("num trees=" + numTreesValues[j]);
                        logger.debug("num features=" + numFeaturesValues[h]);
                        randomForest.setNumFeatures(numFeaturesValues[h]);
                        randomForest.buildClassifier(data);
                        Evaluation eTest = new Evaluation(data);
                        eTest.evaluateModel(randomForest, data);
                        if (eTest.errorRate() < error) {
                            bestMaxDepth = maxDepthValues[i];
                            bestNumTrees = numTreesValues[j];
                            bestNumFeatures = numFeaturesValues[h];
                            error = eTest.errorRate();
                        }
                    }
                }
            }
            logger.debug("Best max depth value: " + bestMaxDepth);
            logger.debug("Best num trees: " + bestNumTrees);
            logger.debug("Best num features: " + bestNumFeatures);

            // train with best combination and show results
            randomForest.setMaxDepth(bestMaxDepth);
            randomForest.setNumFeatures(bestNumFeatures);
            randomForest.setNumTrees(bestNumTrees);
            randomForest.buildClassifier(data);
            SerializationHelper.write(CLASSIFIER_FILE, randomForest);
            Evaluation eTest = new Evaluation(data);
            eTest.evaluateModel(randomForest, data);
            logger.debug(eTest.toSummaryString());
        } catch (Exception e) {
            logger.debug("error training random forest with CSV data", e);
        }
    }

}
