package net.tknika.domo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * Imports rooms from 'rooms' file. The first file contains the headers and then one file per room. Fields:<br>
 * 0 -> Name of the room.<br>
 * 1 -> Openhab code of the room.<br>
 * 2 -> Google calendar id.<br>
 * 3 -> Temperature set-point; if empty a global value will be used (see config file).<br>
 * 4 -> Target flag: 'electricity', 'heating', 'all'. If empty default value will be 'all'<br>
 * 
 * @author acarrascal (acarrascal@daiasolutions.com)
 *
 */
public class RoomsManager {

    private static final String fileName = "rooms";
    private static final Logger logger = Logger.getLogger(RoomsManager.class);

    private Map<String, Room> rooms = new HashMap<String, Room>();
    private double desiredTemp;

    public RoomsManager() {

        init();

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(fileName)));
            String line;

            while ((line = br.readLine()) != null) {
                try {
                    line = line.trim();
                    if (!line.isEmpty())
                    {
                        String[] fields = line.split(",");
                        Room r = new Room();
                        r.setName(fields[0].trim());
                        r.setCode(fields[1].trim());
                        r.setCalendarId(fields[2].trim());

                        String tmp = fields[3].trim();
                        if (tmp.isEmpty())
                            r.setTemperatureSetPoint(desiredTemp);
                        else
                            r.setTemperatureSetPoint(Double.parseDouble(fields[3]));
                        String target = fields[4].trim().toLowerCase();
                        if (target.isEmpty())
                            r.setTarget(Room.Target.all);
                        else
                            r.setTarget(Room.Target.valueOf(fields[4].trim()));

                        rooms.put(r.getName().toLowerCase(), r);
                    }

                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

            }

            br.close();

        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

    }

    private void init() {

        desiredTemp = Config.getDesiredTemp();

    }

    public final Room getRoom(String name) {
        return name != null ? rooms.get(name.trim().toLowerCase()) : null;
    }

    public final Collection<Room> getRooms() {
        return rooms.values();
    }

}
