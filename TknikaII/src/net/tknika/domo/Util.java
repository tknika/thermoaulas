package net.tknika.domo;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Util {

    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1.get(Calendar.DAY_OF_YEAR) == cal2
                .get(Calendar.DAY_OF_YEAR));
    }

    public static Date addDays(Date date, int amount) {
        return add(date, Calendar.DAY_OF_MONTH, amount);
    }

    public static Date add(Date date, int calendarField, int amount) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(calendarField, amount);
        return c.getTime();
    }

    public static void timeFieldsToMinimum(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        date.getTimeInMillis();
    }

    /**
     * Sets date to 23:59 59s 999ms (end of day)
     * 
     * @param date
     */
    public static void timeFieldsToMaximum(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, 23);
        date.set(Calendar.MINUTE, 59);
        date.set(Calendar.SECOND, 59);
        date.set(Calendar.MILLISECOND, 999);
        date.getTimeInMillis();
    }

    public static Date timeFieldsToMaximum(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        timeFieldsToMaximum(cal);
        return cal.getTime();
    }

    public static Date timeFieldsToMinimum(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        timeFieldsToMaximum(cal);
        return cal.getTime();
    }

    public static String toString(Date d) {

        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(d);

        return timeToString(cal);

    }

    public static String timeToString(Calendar cal) {
        return cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
    }

}
