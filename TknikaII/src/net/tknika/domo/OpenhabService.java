package net.tknika.domo;

import java.io.IOException;
import java.io.StringReader;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class OpenhabService {

    private static final Logger logger = Logger.getLogger(OpenhabService.class);
    private static final int MAX_RETRIES = 2;

    private String username;
    private String password;
    private String getTokenUrl;
    private String electricitySwitchUrl;
    private String heatSwitchUrl;
    private String electricitySwitchStateUrl;
    private String heatSwitchStateUrl;

    private String getDataUrl;

    private String temperatureSensorUrl;
    private String presenceSensorUrl;
    private String outsideTemperatureUrl;
    private String centralHeatingOn;

    public OpenhabService() {

        username = Config.getUsername();
        password = Config.getPassword();

        getTokenUrl = Config.getGetTokenUrl();
        heatSwitchUrl = Config.getHeatSwitchUrl();
        heatSwitchStateUrl = Config.getHeatSwitchStateUrl();
        getDataUrl = Config.getGetDataUrl();
        electricitySwitchUrl = Config.getElectricitySwitchUrl();
        electricitySwitchStateUrl = Config.getElectricitySwitchStateUrl();
        temperatureSensorUrl = Config.getTemperatureSensorUrl();
        presenceSensorUrl = Config.getPresenceSensorUrl();
        outsideTemperatureUrl = Config.getOutsideTemperatureUrl();
        centralHeatingOn = Config.getCentralHeatingOn();

    }

    public static void main(String[] args) {
        Config.init();
        Room room = new Room();
        room.setName("Nafarroa Aretoa");
        room.setCode("nafarroa103x");

        OpenhabService deviceService = new OpenhabService();

        DomoticData data = deviceService.getRoomData(room);

        data.getClass();

        // deviceService.turnOnAllExceptHeat(room);

    }

    public DomoticData getRoomData(Room room) {
        // logger.info(room.getName() + ": openhab - get room domotic data");
        DomoticData data = new DomoticData(new Date().getTime());

        try {
            ItemState is = getItemState(room, presenceSensorUrl);
            data.setPresence(is.equals(ItemState.OPEN));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        try {
            ItemState is = getItemState(room, temperatureSensorUrl);
            data.setTemperature(Double.parseDouble(is.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        data.setExternalTemperature(getOutsideTemperature());

        return data;
    }

    public void getToken() throws ClientProtocolException, IOException, JSONException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost post = new HttpPost(getTokenUrl);
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("username", username));
        params.add(new BasicNameValuePair("password", password));
        post.setEntity(new UrlEncodedFormEntity(params));

        httpclient.close();
    }

    public ItemState getItemState(String itemUrl) {
        try {
            return getItemState(null, itemUrl);
        } catch (InterruptedException e) {
            return null;
        }
    }

    public ItemState getItemState(Room room, String itemUrl) throws InterruptedException {

        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));
        CloseableHttpClient httpClient =
                HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();

        try {

            HttpGet httpGet;
            if (room != null)
                httpGet = new HttpGet(MessageFormat.format(itemUrl, room.getCode()));
            else
                httpGet = new HttpGet(itemUrl);
            CloseableHttpResponse response;
            int retry = 0;
            do {
                if (retry > 0)
                    Thread.sleep(1000);
                retry++;
                response = httpClient.execute(httpGet);
            } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);

            if (response.getStatusLine().getStatusCode() != 200) {
                logger.error("Error when connecting to Openhab: " + room.getCode());
                return null;
            }

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = null;
            try {
                db = dbFactory.newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(EntityUtils.toString(response.getEntity())));

                try {
                    Document doc = db.parse(is);

                    NodeList list = doc.getElementsByTagName("item");
                    if (list != null) {

                        for (int i = 0; i < list.getLength(); i++) {
                            Node n = list.item(i);

                            NodeList nl = n.getChildNodes();
                            for (int j = 0; j < nl.getLength(); j++) {
                                if (nl.item(j).getNodeName().equalsIgnoreCase("state")) {
                                    try {
                                        return new ItemState(nl.item(j).getFirstChild().getNodeValue());
                                    } catch (Exception e) {

                                    }
                                }
                            }

                        }
                    }

                }

                catch (SAXException e) {
                    logger.error(e.getMessage());
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            } catch (ParserConfigurationException e1) {
                logger.error(e1.getMessage());
            }

        } catch (ClientProtocolException e) {
            logger.error("error getting Outside Temperature", e);
        } catch (IOException e) {
            logger.error("error getting Outside Temperature", e);
        } catch (JSONException e) {
            logger.error("error getting Outside Temperature", e);
        }
        return null;

    }

    public void setItemState(Room room, String itemUrl, ItemState state) {

        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));
        CloseableHttpClient httpClient =
                HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();

        try {

            HttpPut httpPost = new HttpPut(MessageFormat.format(itemUrl, room.getCode()));
            httpPost.addHeader("Content-Type", "text/plain");

            httpPost.setEntity(new StringEntity(state.toString()));
            CloseableHttpResponse response;
            int retry = 0;
            do {
                retry++;
                response = httpClient.execute(httpPost);
            } while (retry < MAX_RETRIES && response.getStatusLine().getStatusCode() != 200);
        } catch (ClientProtocolException e) {
            logger.error("error turnOnAll", e);
        } catch (IOException e) {
            logger.error("error turnOnAll", e);
        }
    }

    public Double getOutsideTemperature() {
        // logger.info("openhab - get Outside Temperature");

        ItemState is = getItemState(outsideTemperatureUrl);

        try {
            return Double.parseDouble(is.toString());

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }

    }

    public void checkRoomElectricityState(Room room, ItemState state) {

        try {
            ItemState currentState = getItemState(room, electricitySwitchUrl);

            if (!currentState.equals(state)) {
                setItemState(room, electricitySwitchStateUrl, state);
                logger.info(room.getName() + ": setting electricity to: " + state.toString());
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    public void checkRoomHeatingState(Room room, ItemState state) {

        try {
            ItemState currentState = getItemState(room, heatSwitchUrl);

            if (!currentState.equals(state)) {
                setItemState(room, heatSwitchStateUrl, state);
                logger.info(room.getName() + ": setting heating to: " + state.toString());
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    public boolean isCentralHeatingOn() {

        if (centralHeatingOn == null)
            return true;

        try {
            ItemState is = getItemState(centralHeatingOn);
            Double val = Double.parseDouble(is.toString());

            if (val != null && val != 0)
                return true;
            return false;

            // return is.equals(ItemState.ON);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

}
