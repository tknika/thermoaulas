package net.tknika.domo;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Timer;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.apache.log4j.Logger;

public class Main implements Daemon {

    private static final Logger logger = Logger.getLogger(Main.class);

    private static Timer timer;

    public static void main(String[] args) {

    }

    @Override
    public void init(DaemonContext arg0) throws DaemonInitException, Exception {
        try {

            Config.init();

            int minutes = Config.getCheckEachMinutes();
            if (minutes > 0) {

                timer = new Timer();
                long millisToNextMinute = getMillisToNextMinute();
                timer.schedule(new MainTask(), millisToNextMinute, minutes * 60 * 1000);
                logger.info("Timer launched");
            }

        } catch (Throwable e) {
            logger.error(e.getMessage());
        }
    }

    // public void init(String[] args) {
    //
    //
    //
    // }

    public void start() {
        // main(null);
    }

    public void stop() {
        if (timer != null) {
            timer.cancel();
        }

    }

    public void destroy() {

    }

    private static long getMillisToNextMinute() {
        Calendar cal = GregorianCalendar.getInstance();
        int sec = cal.get(Calendar.SECOND);
        return (60 - sec) * 1000;
    }

}
