package net.tknika.domo;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Config {

    private static final String File = "configuration.properties";
    private static final Logger logger = Logger.getLogger(Config.class);

    private static final double defaultDesiredTemp = 22;

    private static double desiredTemp;
    private static int startAllMinutesBefore;
    private static int finishAllMinutesLater;
    private static int checkEachMinutes;
    private static int minimumInstances = 10;
    private static int firstReservationHeatingAnticipation;

    private static long minHeatingWindowTime = 5 * 60 * 1000;
    private static long maxHeatingWindowTime = 120 * 60 * 1000;
    private static long minAnticipationNeeded = 5 * 60 * 1000;

    // Google Calendar
    private static String appName;
    private static String accountID;
    private static String privateKey;

    // Openhab
    private static String username;
    private static String password;
    private static String getTokenUrl;
    private static String getDataUrl;
    private static String electricitySwitchUrl;
    private static String heatSwitchUrl;
    private static String electricitySwitchStateUrl;
    private static String heatSwitchStateUrl;
    private static String temperatureSensorUrl;
    private static String presenceSensorUrl;
    private static String outsideTemperatureUrl;
    private static String centralHeatingOnUrl;

    // ai
    private static int maxDepth = 6;
    private static int numTrees = 500;

    public static void init() {
        PropertyConfigurator.configure("log4j.properties");

        try {

            Properties properties = new Properties();

            properties.load(new FileInputStream(File));

            try {
                desiredTemp = Double.parseDouble(properties.getProperty("domotic.desiredTemp"));
            } catch (Exception e) {
                desiredTemp = defaultDesiredTemp;
            }

            try {
                checkEachMinutes = Integer.parseInt(properties.getProperty("domotic.reservationCheckEachMinutes"));
            } catch (Exception e) {
            }
            try {
                startAllMinutesBefore = Integer.parseInt(properties.getProperty("domotic.startAllMinutesBefore"));
            } catch (Exception e) {
            }
            try {
                finishAllMinutesLater = Integer.parseInt(properties.getProperty("domotic.finishAllMinutesLater"));
            } catch (Exception e) {
            }
            try {
                minimumInstances = Integer.parseInt(properties.getProperty("domotic.minimumInstances"));
            } catch (Exception e) {
            }
            try {
                firstReservationHeatingAnticipation = Integer.parseInt(properties
                        .getProperty("domotic.firstReservationHeatingAnticipation"));
            } catch (Exception e) {
            }

            try {
                minHeatingWindowTime = Long.parseLong(properties.getProperty("domotic.minHeatingWindowTime"));
            } catch (Exception e) {
            }
            try {
                maxHeatingWindowTime = Long.parseLong(properties.getProperty("domotic.maxHeatingWindowTime"));
            } catch (Exception e) {
            }

            try {
                minAnticipationNeeded = Long.parseLong(properties.getProperty("domotic.minAnticipationNeeded"));
            } catch (Exception e) {
            }

            try {
                maxDepth = Integer.parseInt(properties.getProperty("ai.maxDepth"));
            } catch (Exception e) {
            }
            try {
                numTrees = Integer.parseInt(properties.getProperty("ai.numTrees"));
            } catch (Exception e) {
            }

            appName = properties.getProperty("gcal.appname");
            accountID = properties.getProperty("gcal.accountID");
            privateKey = properties.getProperty("gcal.privateKey");

            username = properties.getProperty("openhab.gateway.username");
            password = properties.getProperty("openhab.gateway.password");

            getTokenUrl = properties.getProperty("openhab.gateway.url.getToken");
            heatSwitchUrl = properties.getProperty("openhab.gateway.url.heatSwitchUrl");
            heatSwitchStateUrl = properties.getProperty("openhab.gateway.url.heatSwitchStateUrl");
            electricitySwitchUrl = properties.getProperty("openhab.gateway.url.electricitySwitchUrl");
            electricitySwitchStateUrl = properties.getProperty("openhab.gateway.url.electricitySwitchStateUrl");
            getDataUrl = properties.getProperty("openhab.gateway.url.getData");

            temperatureSensorUrl = properties.getProperty("openhab.gateway.url.temperatureSensorUrl");
            presenceSensorUrl = properties.getProperty("openhab.gateway.url.presenceSensorUrl");
            outsideTemperatureUrl = properties.getProperty("openhab.outside_temperature");
            centralHeatingOnUrl = properties.getProperty("openhab.central_heating_on");

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public static final double getDesiredTemp() {
        return desiredTemp;
    }

    public static final int getStartAllMinutesBefore() {
        return startAllMinutesBefore;
    }

    public static final int getFinishAllMinutesLater() {
        return finishAllMinutesLater;
    }

    public static final int getCheckEachMinutes() {
        return checkEachMinutes;
    }

    public static final String getAppName() {
        return appName;
    }

    public static final String getAccountID() {
        return accountID;
    }

    public static final String getPrivateKey() {
        return privateKey;
    }

    public static final String getUsername() {
        return username;
    }

    public static final String getPassword() {
        return password;
    }

    public static final String getGetTokenUrl() {
        return getTokenUrl;
    }

    public static final String getGetDataUrl() {
        return getDataUrl;
    }

    public static final String getOutsideTemperatureUrl() {
        return outsideTemperatureUrl;
    }

    public static int getMinimumInstances() {
        return minimumInstances;
    }

    public static String getCentralHeatingOn() {
        return centralHeatingOnUrl;
    }

    public static int getFirstReservationHeatingAnticipation() {
        return firstReservationHeatingAnticipation;
    }

    public static String getElectricitySwitchUrl() {
        return electricitySwitchUrl;
    }

    public static String getHeatSwitchUrl() {
        return heatSwitchUrl;
    }

    public static String getHeatSwitchStateUrl() {
        return heatSwitchStateUrl;
    }

    public static String getElectricitySwitchStateUrl() {
        return electricitySwitchStateUrl;
    }

    public static String getTemperatureSensorUrl() {
        return temperatureSensorUrl;
    }

    public static String getPresenceSensorUrl() {
        return presenceSensorUrl;
    }

    public static final long getMinHeatingWindowTime() {
        return minHeatingWindowTime;
    }

    public static final void setMinHeatingWindowTime(long minHeatingWindowTime) {
        Config.minHeatingWindowTime = minHeatingWindowTime;
    }

    public static final long getMaxHeatingWindowTime() {
        return maxHeatingWindowTime;
    }

    public static final void setMaxHeatingWindowTime(long maxHeatingWindowTime) {
        Config.maxHeatingWindowTime = maxHeatingWindowTime;
    }

    public static final int getMaxDepth() {
        return maxDepth;
    }

    public static final void setMaxDepth(int maxDepth) {
        Config.maxDepth = maxDepth;
    }

    public static final int getNumTrees() {
        return numTrees;
    }

    public static final void setNumTrees(int numTrees) {
        Config.numTrees = numTrees;
    }

    public static long getMinAnticipationNeeded() {
        return minAnticipationNeeded;
    }

    public static void setMinAnticipationNeeded(long minAnticipationNeeded) {
        Config.minAnticipationNeeded = minAnticipationNeeded;
    }

}
