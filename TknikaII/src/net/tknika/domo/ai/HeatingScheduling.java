package net.tknika.domo.ai;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class HeatingScheduling implements Serializable {

    private static final long serialVersionUID = -2449251385234378058L;

    private static final String file = "heating_scheduling.obj";
    public static final int MaxSchedulings = 1000;

    private Map<String, ScheduledHeating> scheduling;

    public HeatingScheduling() {
        super();
        scheduling = new HashMap<String, ScheduledHeating>();
    }

    public final Map<String, ScheduledHeating> getScheduling() {
        return scheduling;
    }

    public final void setScheduling(Map<String, ScheduledHeating> scheduling) {
        this.scheduling = scheduling;
    }

    public synchronized void save() {
        try
        {
            FileOutputStream fileOut = new FileOutputStream(file);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(this);
            out.close();
            fileOut.close();

        } catch (IOException i)
        {
            i.printStackTrace();
        }
    }

    public synchronized static HeatingScheduling getInstance() {
        try
        {

            File f = new File(file);
            if (!f.exists()) {
                return new HeatingScheduling();
            }

            FileInputStream fileIn = new FileInputStream(file);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            HeatingScheduling scheduling = (HeatingScheduling) in.readObject();
            in.close();
            fileIn.close();

            if (scheduling.getScheduling() == null)
                scheduling.setScheduling(new HashMap<String, ScheduledHeating>());

            return scheduling;
        } catch (IOException i)
        {
            i.printStackTrace();
            return null;
        } catch (ClassNotFoundException c)
        {
            c.printStackTrace();
            return null;
        }
    }

}
