package net.tknika.domo.ai;

import java.io.Serializable;

import net.tknika.domo.DomoticData;

public class ScheduledHeating implements Serializable {

    private static final long serialVersionUID = -4899314248580243779L;

    public static final String Anticipation = "Anticipation";

    private String id;
    private long start;
    private long end;

    private DomoticData domoticDataAtStart;
    private boolean finished;
    private long anticipation;

    public final String getId() {
        return id;
    }

    public final void setId(String id) {
        this.id = id;
    }

    public final long getStart() {
        return start;
    }

    public final void setStart(long start) {
        this.start = start;
    }

    public final long getEnd() {
        return end;
    }

    public final void setEnd(long end) {
        this.end = end;
    }

    public final boolean inHeatingWindowTime(long date) {
        return date >= start && date <= end;
    }

    public final DomoticData getDomoticDataAtStart() {
        return domoticDataAtStart;
    }

    public final void setDomoticDataAtStart(DomoticData domoticDataAtStart) {
        this.domoticDataAtStart = domoticDataAtStart;
    }

    public final boolean isFinished() {
        return finished;
    }

    public final void setFinished(boolean finished) {
        this.finished = finished;
    }

    public long getAnticipation() {
        return anticipation;
    }

    public void setAnticipation(long anticipation) {
        this.anticipation = anticipation;
    }

}
