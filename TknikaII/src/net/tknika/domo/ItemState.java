package net.tknika.domo;

public class ItemState {

    public static final ItemState ON = new ItemState("ON");
    public static final ItemState OFF = new ItemState("OFF");
    public static final ItemState UNDEFINED = new ItemState("UNDEFINED");
    public static final ItemState OPEN = new ItemState("OPEN");
    public static final ItemState CLOSED = new ItemState("CLOSED");

    private final String val;

    public ItemState(String val) {
        this.val = val != null ? val.trim() : null;

    }

    public boolean equals(Object val2) {
        if (val2 != null && val2 instanceof ItemState) {
            if (((ItemState) val2).val == null || val == null)
                return false;
            return ((ItemState) val2).val.equalsIgnoreCase(val);
        }
        return false;
    }

    public String toString() {
        return val;
    }

}
